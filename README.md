```
 ____            _             _       ____            _       _       
|  _ \ __ _ _ __| | _____ _ __( )___  / ___|  ___ _ __(_)_ __ | |_ ___ 
| |_) / _` | '__| |/ / _ \ '__|// __| \___ \ / __| '__| | '_ \| __/ __|
|  __/ (_| | |  |   <  __/ |    \__ \  ___) | (__| |  | | |_) | |_\__ \
|_|   \__,_|_|  |_|\_\___|_|    |___/ |____/ \___|_|  |_| .__/ \__|___/
                                                        |_|           
```
---
This is my scripts repository!  I have things I do often.  And I don't believe in putting them
all in my `.bashrc` file, as it makes it unbearably long.  Plus, I like to write them in different
languages.  Some are shell scripts, some are python scripts, and I’ll even have some that are
written in [owl lisp][owl].

Feel free to copy these files as you see fit.  Though I believe in the GPL, I don't really care
about these scripts beyond my personal use for them.  They're just here for other people to reference.
See the [license](/LICENSE) for more details.


Requirements
============
* a UNIX system (duh)
* [owl lisp][owl]: required for compiling the certain programs
* [Plan 9 from User Space][p9p]: A lot of the scripts that I run will be launched inside my text editor,
  [acme](http://acme.cat-v.org/), so they'll be useless without it.  Also some scripts are written in
  the [rc shell][rc], the default shell in Plan 9.  It's very nice.
* Python 3
* Any POSIX complient shell.

[owl]: https://gitlab.com/owl-lisp/owl
[p9p]: https://9fans.github.io/plan9port/
[rc]: http://doc.cat-v.org/plan_9/4th_edition/papers/rc
  